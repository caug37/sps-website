var app = angular.module("myApp", []);

app.controller('MainController', ['$scope',function($scope) {
	$scope.showpastevents = false;
	$scope.show = function() {$scope.showpastevents = true};
	$scope.hide = function() {$scope.showpastevents = false};

	$scope.today = new Date();
	$scope.upcoming = function(item) {return (item.datetime > $scope.today)};
	$scope.past = function(item) {return (item.datetime < $scope.today)};

	$scope.eventheaders = ['Event', 'Type', 'Date', 'Time', 'Location', 'Details'];
	$scope.pasteventheaders = ['Event', 'Type', 'Date', 'Time', 'Location', 'Details'];

	$scope.events = [

{
name: 'Fall Officer Elections',
type: 'Academic/Social',
date: new Date(2015,4, 8, 6),
dateend: new Date(2015,4,8,7),
location: '251 LeConte',
description: 'SPS will be holding elections to decide officers for the upcoming fall semester. All members are encouraged to attend and if you cannot make it you should drop by the SPS room sometime before to submit an absentee ballot.'},
	



{
name: 'Department Picnic',
type: 'Social',
date: new Date(2015,4,8,12,30),
dateend: new Date(2015,4,8,13,30),
location: '375 LeConte',
description: 'The department is hosting its annual spring picnic and all students are invited to attend. Get tickets for it or you can volunteer and get free food at 366 LeConte. ',
},



{
name: 'Faculty Lunch with Hartmut Haeffner   ',
type: 'Academic/Social',
date: new Date(2015,4,6,12,30),

dateend: new Date(2015,4,6,1,30),
location:" 375 LeConte Hall",
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Professor Haeffner, whose research is in ion trapping in order to investigate aspects of quantum physics and quantum information.',
},



{
name: 'Project Discussions',
type: 'Academic/Social',
date: new Date(2015,4,1,18),

dateend: new Date(2015,4,1,19),

location: '251 LeConte',
description: 'SPS will be hosting a meeting to talk about future projects.',
},
{
name: 'Final Barbecue',
type: 'Social',
date:new Date(2015,4,1,12),
dateend: new Date(2015,4,1,14),

location: 'LeConte Courtyard',
description: 'SPS is hosting a barbecue where we will be serving hot dogs, hamburgers, veggie burgers, turkey burgers, hot links, various drinks and goodies. Help us raise money for our club!',
},
{
name: 'STEM Mixer ',
type:' Academic/Social',
date: new Date(2015,3,30,17),
dateend: new Date(2015,3,30,19),

location: '375 LeConte Hall',
description: 'This event is for all students. It will be a mixer for SPS and Atmospheric Sciences students to get together and hang out.',
},

{
name: ' Reading Room Open House   ',
type: 'Academic',
date: new Date(2015,4,29,11),	 
dateend: new Date(2015,4,29,13),

location: '1st Floor LeConte'	,
description: 'Come tour the new reading room on the first floor. We will also be getting a new SPS room',
},


{
name: 'Faculty Lunch with Alex Filippenko  ',
type: 'Academic/Social',
date: new Date(2015,3,22,12,30), 

dateend:  new Date(2015,3,22,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Professor Filippenko, whose research is in supernova and is involved in science outreach.',
},
{
name: 'Faculty Lunch with Gibor Basri',
type: 'Academic/Social',
date: new Date(2015,3,16,12,30),
dateend: new Date(2015,3,16,1,30),

location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Professor Basri, whose research is focused on stellar magnetic activity, star formation, low mass stars, brown dwarfs, high resolution spectroscopy, radiative transfer, and planetary systems.',

},

{
name: 'Faculty Lunch with Steve Beckwith  ',
type: 'Academic/Social',
date: new Date(2015,3,2,12,30),
dateend: new Date(2015,3,2,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Professor Beckwith, whose research is focused on cosmology, galactic evolution, and galactic luminosity function.',
},







{
name: 'Zone 18 Meeting',
type: 'Social/Professional',
date: new Date(2015,2,20) ,
dateend: new Date(2015,2,22),
location: 'LeConte Hall',
description: 'Come to the zone meeting for Zone 18 of the National SPS. The meeting will feature talks by Berkeley Professors as well as a tour of the Advanced Light Source Lab and a poster session featuring undergraduate research. Find out more details by visiting <a href="./zonemeeting">here!</a>',

},

{
name: 'SPS Barbecue',
type: 'Social',
date: new Date(2015,2,20,12),
dateend: new Date(2015,2,20,2),
location: 'LeConte Courtyard',
description: 'SPS is hosting a barbecue where we will be serving hot dogs, hamburgers, veggie burgers, turkey burgers, hot links, various drinks and goodies. Help us raise money for our club!',

},

{
name: 'SPS Student Colloquium with Aditya',
type: 'Academic',
date: new Date(2015,2,10,12),
dateend: new Date(2015,2,10,2),
location: 'LeConte Courtyard',
description: 'SPS is hosting student led collquiua where undergraduates present their research giving others insight on ground breaking topics in different areas of physics.',
},


{
name: 'Faculty Lunch with Dan Stamper Kurn  ',
type: 'Academic/Social',
date: new Date(2015,1,24,12,30),
dateend: new Date(2015,1,24,1,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Professor Stamper Kurn, whose research is in Bose Einstein Condensates, cavity QED, and other atomic systems.',

},



{
name: 'Student Colloquium with Kunal Marwaha',
type: 'Social/Academic',
date: new Date(2015,1,17,17,30),
dateend: new Date(2015,1,17,18,30),
location: 'TBD',
description: "SPS is hosting student led collquiua where undergraduates present their research giving others insight on ground breaking topics in different areas of physics. Kunal's talk is named 'Quantum Applications to the Compressive Phase Retrieval Problem'. In it, he will discuss how solutions to this 'compressive phase retrieval problem' 'can use a quantum measurement scheme to reconstruct states, and potential implications thereof",
},

{
name: 'Barbecue',
type: 'Social',
date: new Date(2015,1,13,12), 
dateend: new Date(2015,1,13,14),
location: 'LeConte Courtyard',
description: 'SPS is hosting a barbecue where we will be serving hot dogs, hamburgers, veggie burgers, turkey burgers, hot links, various drinks and goodies. Help us raise money for our club!',
},



{
name: 'Student Colloquium with Mahroud Sayrafi',
type: 'Social/Academic',
date: new Date(2015,1,9,18),
dateend: new Date(2015,1,9,19),
location: 'TBD',
description: 'SPS is hosting student led collquiua where undergraduates present their research giving others insight on ground breaking topics in different areas of physics.',
},

{
name: 'General Meeting',
type: 'Social',
date: new Date(2015,1,6,18),
dateend: new Date(2015,1,6,19),
location: '251 LeConte',
description: 'We will be discussing the upcoming zone meeting, the upcoming student presentations, the first barbecue of the semester, faculty lunches, and equity.',
},

{name: 'COMPASS/SPS Joint Meeting',
type: 'Social',
date: new Date(2015, 0, 30, 11),

dateend: new Date(2015,0,30,12),

location: '251 LeConte',
description: 'SPS and Compass will be meeting to discuss how each can help each other and possible events we could hold together throughout the semester.',

},

{
name: 'NIF Lab Tour',
type: 'Social/Professional',
date: new Date(2015,0,15,9,30),
dateend: new Date(2015,0,15,14),
location: 'Lawrence Livermore National Laboratory',
description: "Come tour the National Ignition Facility at the Lawrence Livermore National Laboratory. It is home to some of the world's foremost plasma physics research.",

},


{
name: 'Department Party',
type: 'Social',
date: new Date(2014,11,12,18),
dateend: new Date(2014,11,12,21),
location: 'International House',
description: "The department throws an annual holiday party and it's actually quite entertaining. Student tickets are 10$ and include great food and entertainment and can be purchased in the department office on 3rd floor of LeConte.",
},


{
name: 'Faculty Lunch with Professor Qiu',
type: 'Academic/Social',
date: new Date(2014,11,5,12,30),
dateend: new Date(2014,11,5,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Professor Qiu, whose research is in magnetic nanostructures.',
},
{
name: 'General Meeting',
type: 'Social',
date: new Date(2014,11,5,18),

dateend: new Date(2014,11,5,19),
location: '251 LeConte Hall',
description: 'SECRET SCHRODINGER',

},
{
name: 'Faculty Lunch with Kam-Biu Luk',
type: 'Academic/Social',
date: new Date(2014,10,24,12,30),
dateend: new Date(2014,10,24,13,30),
location: '375 LeConte Hall',
description: "This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Kam-Biu Luk, who performs research in Particle Experiment.",
},


{
name: 'General Meeting',
type: 'Social',
date: new Date(2014,10,21,12),
dateend: new Date(2014,10,21,14),
location: 'LeConte Courtyard',
description: 'SPS is hosting a barbecue where we will be serving hot dogs, hamburgers, veggie burgers, turkey burgers, hot links, various rinks and goodies. Help us raise money for our club!',
},



{
name: 'Faculty Lunch with Jeff Neaton',
type: 'Academic/Social',
date: new Date(2014,10,20,12,30),

dateend: new Date(2014,10,20,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Jeff Neaton, who performs research in theoretical condensed matter physics, performing computational simuations of complex crystal structures.',
},

{

name: 'General Meeting',
type: 'Social',
date: new Date(2014,10,14,17),
dateend: new Date(2014,10,14,18),
location: '251 LeConte',
description: 'We will discuss the NIF tour, the skit for the holiday party, the gift exchange, and other pressing issues.',
	},



{name: 'Faculty Lunch with Oscar Hallatscheck',
type: 'Academic/Social',
date: new Date(2014,10,12,30),
dateend: new Date(2014,10,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Oscar Hallatscheck, who performs research in Biophysics theory.',},


{
name: 'General Meeting',
type: 'Social',
date: new Date(2014,10,7,17),
dateend: new Date(2014,10,7,18),
location: '251 LeConte',
description: 'We will discuss the MUSA challenge, the zone meeting, and the skit for the holiday party as well as the possibility of a gift exchange.',
	
},


{name: 'Faculty Lunch with Roger Falcone',
type: 'Academic/Social',
date: new Date(2014,10,6,12,30),
dateend: new Date(2014,10,6,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Roger Falcone, who performs research in AMO physics and who is also the director of the ALS lab at LBL.',
},
{
name: 'Faculty Lunch with Stephen Leone',
type: 'Academic/Social',
date: new Date(2014,10,4,12,30),
dateend: new Date(2014,10,4,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Stephen Leone, who performs research in AMO physics.',
},


{
name: 'Faculty Lunch with Dan Kasen ',
type: 'Academic/Social',
date: new Date(2014,9,30,12,30),
dateend: new Date(2014,9,30,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Dan Kasen, who performs research in astrophysical theory.',},

{

name: 'General Meeting',
type: 'Social',
date: new Date(2014,9,24,17),
dateend: new Date(2014,9,24,18) ,
location: '251 LeConte',
description: 'We will discuss the reorganization of undergraduate space, the zone meeting, the next barbecue, and the department holiday party. ',
	
},
{
name: 'Faculty Lunch with Edgar Knobloch',
type: 'Academic/Social',
date: new Date(2014,9,22,12,30),
dateend: new Date(2014,9,22,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Edgar Knobloch, who performs research in Nonlinear dynamics.',
},

{
name: 'General Meeting',
type: 'Social',
date: new Date(2014,9,17,12) ,
dateend: new Date(2014,9,17,14),
location: 'LeConte Courtyard',
description: 'SPS is hosting a barbecue where we will be serving hot dogs, hamburgers, veggie burgers, turkey burgers, hot links, various rinks and goodies. Help us raise money for our club!',

},

{
name: 'Faculty Lunch with Wick Haxton',
type: 'Academic/Social',
date: new Date(2014,9,14,12,30) ,
dateend: new Date(2014,9,14,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Wick Haxton, who performs research in Astrophysical theory regarding solar neutrinos.',
},


{
name: 'General Meeting',
type: 'Social',
date: new Date(2014,9,10,17),
dateend:new Date(2014,9,10,18) ,
location: '251 LeConte',
description: 'We will discuss the blog, the pGRE decal, the tinkering room, faculty lunches, the Bay Area Science Festival, the Zone meeting and the upcoming Barbecue. ',
	

},

{name: 'Faculty Lunch with Frances Hellman',
type: 'Academic/Social',
date: new Date(2014,9,10,12,30),
dateend: new Date(2014,9,10,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Frances Hellman, who performs research in Condensed Matter Experiment.',},
{
name: 'Faculty Lunch with Feng Weng',
type: 'Academic/Social',
date: new Date(2014,9,9,12,30),
dateend: new Date(2014,9,9,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Feng Wang, who performs research in Condensed Matter Experiment.',
},



{
name: 'Faculty Lunch with Dmitry Budker',
type: 'Academic/Social',
date: new Date(2014,8,30,12,30),
dateend: new Date(2014,8,30,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Dmitry Budker, who performs research in AMO Experiment.',
},

{
name: 'General Meeting',
type:  'Social',
date: new Date(2014,8,26,17),
dateend: new Date(2014,8,26,18),
location: '251 LeConte',
description: 'We will discuss ASUC funding, the website, a possible blog,Snack Shack, and the Bay Area Science Festival. ',
	},

{
name: 'Faculty Lunch with Jonathon Wurtele',
type: 'Academic/Social',
date: new Date(2014,8,26,12,30),
dateend: new Date(2014,8,26,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Jonathon Wurtele, who performs research in astro theory.',},


{
name: 'Faculty Lunch with Steve Stahler',
type: 'Academic/Social',
date: new Date(2014,8,19,12,30),
dateend: new Date(2014,8,19,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Steve Stahler, who performs research in astro theory.' },

{
name: 'General Meeting',
type: 'Social',
date: new Date(2014,8,19,17),
dateend: new Date(2014,8,19,18),
location: '251 LeConte',
description: 'We will discuss barbecue successes/failures, the possibility of a NIF tour, IGenSpectrum, and Bay area Science Festival. ',
	
},

{name: 'General Meeting',
type: 'Social',
date: new Date(2014,8,19,15), 
dateend: new Date(2014,8,19,17),
location: 'LeConte Courtyard',
description: 'SPS is hosting a barbecue where we will be serving hot dogs, hamburgers, veggie burgers, turkey burgers, hot links, various rinks and goodies. Help us raise money for our club!',
},

{
name: 'Faculty Lunch with Surjeet Rajendran',
type: 'Academic/Social',
date: new Date(2014,8,11,12,30),
dateend: new Date(2014,8,11,13,30),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Surjeet Rajendran, who performs research in particle theory.',
},
{
name: 'General Meeting',
type: 'Social',
date: new Date(2014,6,26,18),
dateend: new Date(2014,6,26,19),
location: '251 LeConte',
description: 'We will plans for the upcoming year. Topics include projects, collabortion with the Space Exploration Society, COMPASS, SWPS, the Bay Area Science Fest, plans for barbecues, faculty lunches, lab tours, ASUC, possibility of a physics blog, Calapalooza, and Zone meeting ideas. ',
	
},
{
name: 'NIF Lab Tour',
type: 'Social/Professional',
date: new Date(2014,4,9,9,30),
dateend: new Date(2014,4,9,14),
location: 'Lawrence Livermore National Laboratory',
description: "Come tour the National Ignition Facility at the Lawrence Livermore National Laboratorry. It is home to some of the world's foremost plasma physics research.",
},

{

name: 'General Meeting',
type: 'Social',
date: new Date(2014,4,6,18),
dateend: new Date(2014,4,6,20),
location: '195 LeConte Hall',
description: 'We will be discussing officer appliations and plans for next semester. Feel welcome to bring your best board games to socialize a bit before finals and network with other physics majors. As usual, there will be free pizza. We hope to see you all there!',

},

{

name: 'Faculty Lunch with Adrian Lee',
type: 'Academic/Social',
date: new Date(2014,3,30,12,15),
dateend: new Date(2014,3,30,13,15),
location: '468 Birge Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Professor Adrian Lee, whose research is in Astrophysics.',
},



{
name: 'Faculty Lunch with Dan Stamper-Kurn',
type: 'Academic/Social',
date: new Date(2014,3,22,12,15),
dateend: new Date(2014,3,30,13,15),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Professor Dan Stamper-Kern, whose research is in Ultracold Atomic Physics.',
},
{
name: 'General Meeting',
type: 'Social',
date: new Date(2014,3,15,19),
dateend: new Date(2014,3,15,20), 
location: '397 LeConte Hall',
description: 'Discuss general club activities and socialize with other members.',
},


{
name: 'Faculty Lunch with Jeff Neaton',
type: 'Academic/Social',
date: new Date(2014,3,9,12,15) ,
dateend: new Date(2014,3,9,13,15),
location: '324 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Professor Jeff Neaton, whose research is in Condensed Matter physics.',
},

{
name: 'Graduate Student Poster Session',
type: 'Academic',
date:  new Date(2014,3,3,10),
dateend: new Date(2014,3,3,12),
location: 'Basement of Stanley',
description: 'The graduate students will be displaying their research in the basement of Stanley and all are welcome to come!',
},

{
name: 'Exoplanet Talk: The Kepler Mission:Ace Planet Hunter',
type: 'Academic/Professional',
date: new Date(2014,2,20,17),
 dateend:new Date(2014,2,20,20) ,
location: '110 Barrows Hall',
description: 'Professor Basri is a Professor in the UC Berkeley astronomy department and he currently serves as the Vice Chancellor for Equity and Inclusion at Cal. He has an extensive history in the search for extrasolar planets and has received numerous awards for his research. For more information, please refer to Professor Basris website: http://astro.berkeley.edu/~basri/',
},

{
name: 'Faculty Lunch with Adrian Lee',
type: 'Academic/Social',
date:new Date(2014,2,19,12,15),
dateend:  new Date(2014,2,13,15),
location: '324 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Professor Adrian Lee, whose research is in Astrophysics.',
},


{
name: 'Faculty Lunch with David Walrod',
type: 'Academic/Social',
date: new Date(2014,2,14,12,15) ,
dateend:new Date(2014,2,14,12,15) ,
location: '324 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and David Walrod, who is a venture partner at Bridgescale Partners where he leads and manages equity investments in a range of communications/IT-focused products. He received his B.A. in physics from Berkeley and a Ph.D. in solid state physics at MIT. He worked there as a postdoctoral fellow in the Research Lab of Electronics',
},

{
name: 'General Meeting',
type: 'Social',
date: new Date(2014,2,4,19),
dateend: new Date(2014,2,4,20),
location: '397 LeConte Hall',
description: 'Discuss general club activities and socialize with other members. THERE WILL ALSO BE A SCAVENGER HUNT! BE THERE!',

},

{

name: 'Faculty Lunch with Dimitry Budker',
type: 'Academic/Social',
date: new Date(2014,2,4,12,15),
dateend: new Date(2014,2,4,12,15),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Professor Dimitry Budker, whose research is in various types of Atomic Physics.',

	},
{
name: 'Faculty Lunch with Peter Yu',
type: 'Academic/Social',
date: new Date(2014,1,21,12,15),
dateend: new Date(2014,1,21,13,15),
location: '324 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Professor Peter Yu, whose research is in Condensed Matter physics.',
},
	
{
name: 'NASA JPL Robotics Talk + Info Session with Dr. Paul Backes',
type: 'Professional',
date: new Date(2014,1,20,19),
dateend: new Date(2014,1,20,20),
location: '101 Morgan Hall',
description: 'Jet Propulsion Laboratory builds robotic spacecraft that explore the solar system. Dr. Paul Backes will present current robotics efforts at JPL including for flight projects and research activities.',
	
},

{
name: 'Faculty Lunch with John Mather',
type: 'Academic/Social',
date: new Date(2014,1,20,12,15),
dateend: new Date(2014,1,20,13,15),

location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and astrophysicist and Nobel Laureate John Mather, whose work on the COBE satellite with George Smoot helped solidify the big-bang theory of the Universe and is now the project scientist for the James Webb Space Telescope.',
	
},
{
name: 'Faculty Lunch with Bob Jacobsen',
type: 'Academic/Social',
date: new Date(2014,1,11,12,15),
dateend: new Date(2014,1,11,13,15),
location: '375 LeConte Hall',
description: 'This event is for all students. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Dr. Bob Jacobsen, whose research is in Experimental Particle Physics.',
	

},
{

name: 'First General Meeting of the Second Semester',
type: 'Social',
date: new Date(2014,1,4,19),
dateend: new Date(2014,1,4,20),
location: '397 LeConte Hall',
description: 'Plans will be made for upcoming Faculty-student lunches, national lab tours, derpartment BBQs and other possible social events.',
	},


{
name: 'Conference for Undergraduate Women in Physics',
type: 'Academic/Social',
date: new Date(2014,0,17,8),
dateend: new Date(2014,0,20,10),
location: 'LeConte',
description: 'Come to the APS sponsored Conference for Undergraduate Women in Physics between January 17 and January 19! There will be research talks, panel discussions, an SPS booth, and much more! Men are welcome too.',
	},
{
name: 'Advanced Light Source Lab Tour',
type: 'Academic/Professional',
date: new Date(2014,0,7,15),
dateend: new Date(2014,0,7,17),
location: 'TBD',
description: 'Join us on a lab tour in LBNL. ',
	},


{
	name: 'General Meeting',
type: 'Social',
date: new Date(2013,11,8,13),
dateend: new Date(2013,11,8,14),
location: 'Evans 85',
description: 'The meeting time is tentative.',
},
{
name: 'Faculty-Student Lunch with Dr. Roger Falcone',
type: 'Academics/Social',
date:new Date(2013,10,21,12,15) ,
dateend: new Date(2013,10,21,13,15),
location: '375 LeConte Hall',
description: 'This event is for all undergrads. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Dr. Roger Falcone, whose research is in Atomic, Molecular, and Optical physics.',
},
{
name: 'General Meeting',
type: 'Social',
date: new Date(2013,10,17,13),
dateend: new Date(2013,10,17,14) ,
location:' Meet by the southwest door to Evans',
description: 'Feel free to drop by if you are interested in getting involved or just want to see what is going on!',
	
},
{
name: 'Faculty Lunch with Melvin Pomerantz',
type: 'Academic/Social',
date: new Date(2013,10,8,13),
dateend: new Date(2013,10,8,14) ,
location:' 375 LeConte Hall',
description: 'He is a visiting researcher currently rworking at LBNL. He researched solid state physics at the IBM Research Lab in New York.',
	},

{
name: 'Cal SPS Exhibit at BASF Discovery Days',
type: 'Outreach',
date: new Date(2013,10,2,11),
dateend: new Date(2013,10,2,16),
location: 'AT&T Park',
description: 'Want to introduce people to teh wonders of physics? Have a Saturday afternoon to burn? Get some cool experience by volunteering to help out at our booth at the Bay Area Science Festival. Email us if you are interested.',
	
},
{
name: 'Faculty-Student Lunch with Dr. Bob Jacobsen',
type: 'Academics/Social',
date: new Date(2013,9,22,12,15),
dateend: new Date(2013,9,22,13,15),
location: '375 LeConte Hall',
description: 'This event is for all undergrads. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Dr. Bob Jacobsen, a particle physicist.',
	},
{

name: 'General Meeting',
type: 'Social',
date: new Date(2013,9,20,13),
dateend: new Date(2013,9,22,14),
location: 'LeConte Courtyard',
description: 'We will make the sub-groups official, assign tasks in these sub-groups, and discuss and decide on new ideas.',
	

},
{
name: 'Career Lunch with Scott Shenker',
type: 'Professional/Social',
date: new Date(2013,9,16,12) ,
dateend: new Date(2013,9,16,13),
location: '375 LeConte Hall',
description: 'Currently a professor in EECS here at Berkeley, Dr. Scott Shenker began his extensive career in as a physicist. From start-ups to papers, his physics grounding has given him a unique perspective in academia and industry.',
	
},
{
name: 'Career Lunch with Matt Kowitt',
type: 'Professional/Social',
date: new Date(2013,9,9,12,15) ,
dateend: new Date(2013,9,22,13,15),
location: '375 LeConte Hall',
description: "Matt Kowitt works at Stanford Research Systems. Come find out what it's like to work in industry!",
	
},
{
	name: 'Faculty-Student Lunch (with Dr. Yury Kolomensky)',
	type: 'Academics/Social',
	date: new Date(2013,8,27,12,15),
	dateend: new Date(2013,8,27,13,15) ,	
	location: '375 LeConte Hall',
	description: 'This event is for all undergrads. Want free pizza? Want to interact with a professor? Just interested in physics? Then, come by and enjoy lunch with fellow physics students and Dr. Yury Kolomensky, an experimental particle physicist.',
	},	

{
name: 'General Meeting',
type: 'Social',
date: new Date(2013,8,27,8,30),
dateend: new Date(2013,8,27,9,30),
location: '129 Barrows',
description: '',
}
];


}]
);


